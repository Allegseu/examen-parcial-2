console.log("Examen - 2P");

// funcion que permite el registro de un nuevo usuario
function register() {
  // funcion para crear objetos de tipo cliente
  function Client(cedula, apellido, nombre, direccion, telefono, email) {
    //   atributos del objeto cliente
    this.cedula = cedula;
    this.apellido = apellido;
    this.nombre = nombre;
    this.direccion = direccion;
    this.telefono = telefono;
    this.email = email;
  }
  // declaramos un array para guardar temporalmente los datos de los usuarios
  let dataBase = [];
  //   se capturan los valores ingresados por el usuario en los input
  let cedulaInput = document.getElementById("cedula").value;
  let apellidoInput = document.getElementById("apellido").value;
  let nombreInput = document.getElementById("nombre").value;
  let direccionInput = document.getElementById("direccion").value;
  let telefonoInput = document.getElementById("telefono").value;
  let emailInput = document.getElementById("email").value;
  // comprueba si algun input obligatorio esta vacio
  if (
    blank(cedulaInput) ||
    blank(apellidoInput) ||
    blank(nombreInput) ||
    blank(direccionInput) ||
    blank(telefonoInput) ||
    blank(emailInput)
  ) {
    alert("Llene todos los campos obligatorios para realizar su registro");
  } else {
    //  instanciamos un nuevo objeto y llamamos a la funcion que crea el objeto, le pasamos como parametros las variables donde se guardaron los valores ingresados
    let newClient = new Client(
      cedulaInput,
      apellidoInput,
      nombreInput,
      direccionInput,
      telefonoInput,
      emailInput
    );
    // agregamos nuestro objeto al array donde se guardara temporalmente
    dataBase.push(newClient);
    console.log(dataBase);
  }
}

// funcion que comprueba si un input esta vacio
function blank(par) {
  if (par === "") {
    return true;
  } else {
    return false;
  }
}
